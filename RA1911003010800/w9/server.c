#include<stdio.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<unistd.h>
#include<netdb.h>
#include<stdlib.h>
#include<string.h>
#include<arpa/inet.h>
#include<time.h>

#define MAX 1000

int main()			                                                    //parameters that carry/have1.argc- no of of CLI commands 2. argv- CLI commands
{
    char *ip = "127.0.0.1";
    int port = 8080;
    FILE *outputf;
    
    int socket1;                                                        //used for creating socket and taking return val;
    socklen_t clil;                                                     //data type holding client struct size;
    char reccomm[MAX], output[MAX];
    struct sockaddr_in servad,cliad;	                                //struct defined in netinet.h used for holding info about connection, like port,ip address

    socket1=socket(AF_INET, SOCK_DGRAM, 0);		                        //Datagram socket defined following AF_INET family of ip addressing(IPv4)
    if(socket1<0)
        printf("\nSocket not created.");
    else
        printf("\nSocket created.");

    clil=sizeof(cliad);

    bzero(&servad, sizeof(servad));     
    servad.sin_family=AF_INET;			                                                            //Struct variable (int)containing type of ip addressing followed
    servad.sin_port=port;			                                                                //Struct var (int) containing port number
    servad.sin_addr.s_addr=inet_addr(ip);		                                                    //struct var(struct) containing ip address(any ip add as of pre-compiling)
    
    
    int binded=bind(socket1,(struct sockaddr*)&servad, sizeof(servad));	                            //to assign/reserve servad struct a port
    if(binded!=0)
        printf("\nBinding unsuccesful.");
    else
        printf("\nBinding succesful.\n\n");
    
    
    
    while(1)
    {
        bzero(reccomm, sizeof(reccomm));						                                    //delete any existing contents of received command char array
        recvfrom(socket1, reccomm, sizeof(reccomm), 0, (struct sockaddr*)&cliad, &clil);	        //receive commands from client with correct address in buffer
         
        outputf=popen(reccomm, "r");
        if(outputf==NULL)
            printf("Command not executed.");
        else
            fgets(output, sizeof(output), outputf);
         
        printf("Command received.\nCommand executed is- %s\n", reccomm);
         
        sendto(socket1, output, sizeof(output), 0, (struct sockaddr*)&cliad, clil); 
    }
    
     return 0;
}