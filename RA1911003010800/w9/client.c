#include<stdio.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<unistd.h>
#include<netdb.h>
#include<stdlib.h>
#include<string.h>
#include<arpa/inet.h>

#define MAX 1000

int main()
{
    char *ip = "127.0.0.1";
    int port = 8080;
    
    int socket1;
    socklen_t servl;
    char sentcomm[MAX],output[MAX];
    struct sockaddr_in servad, clientad;

    socket1=socket(AF_INET, SOCK_DGRAM, 0);
    if(socket<0)
        printf("\nSocket not created.");
    else
        printf("\nSocket created.");
   
    servl=sizeof(servad);
    
    servad.sin_family=AF_INET;
    servad.sin_port=port;
    servad.sin_addr.s_addr=inet_addr(ip);

    int binded=bind(socket1, (struct sockaddr*)&servad, sizeof(servad));
    if(binded<0)
        printf("\nBinding succesful.");
    else
        printf("\nBinding unsuccesful.");

    while(1)
    {

        printf("\nEnter command to be executed-");
        fgets(sentcomm, sizeof(sentcomm), stdin);					                        	//Receive commands from standard input file, i.e, CLI input
        sendto(socket1, sentcomm, sizeof(sentcomm), 0, (struct sockaddr*)&servad, servl);		//Send commands to server via socket (identify derver using its struct)
        printf("\nCommand sent.");
         
        recvfrom(socket1, output, sizeof(output), 0, (struct sockaddr*)&servad, &servl);			//Receive output from server
        printf("\nCommand output-\n%s", output);
    }

    return 0;
}