#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include "netdb.h"
#include "arpa/inet.h"


#define h_addr h_addr_list[0] 

#define PORT 9002 
#define MAX 1000  

int main(){
    char serverResponse[MAX];
    char clientResponse[MAX];

    //creating a socket
    int socketDescriptor = socket(AF_INET, SOCK_STREAM, 0);

    //placeholder for the hostname and my ip address
    char hostname[MAX], ipaddress[MAX];
struct hostent *hostIP; //placeholder for the ip address
//if the gethostname returns a name then the program will get the ip address
if(gethostname(hostname,sizeof(hostname))==0){
    hostIP = gethostbyname(hostname);//the netdb.h fucntion gethostbyname
}else{
printf("ERROR:FCC4539 IP Address Not ");
}

struct sockaddr_in serverAddress;
serverAddress.sin_family = AF_INET;
serverAddress.sin_port = htons(PORT);
serverAddress.sin_addr.s_addr = INADDR_ANY; 

connect(socketDescriptor, (struct sockaddr *)&serverAddress, sizeof(serverAddress));

// getting the address port and remote host
    printf("\nLocal Host: %s\n", inet_ntoa(*(struct in_addr*)hostIP->h_addr));
    printf("Local Port: %d\n", PORT);
    printf("Remote Host: %s\n", inet_ntoa(serverAddress.sin_addr));
    printf("-------------Communication Starts-----------------\n");

    while (1)
    {   //recieve the data from the server
        recv(socketDescriptor, serverResponse, sizeof(serverResponse), 0);
            //recieved data from the server successfully 
            printf("\nMessage from Server : %s", serverResponse);
        
    printf("\nEnter message for Server :");
    scanf("%s", clientResponse);
    send(socketDescriptor, clientResponse, sizeof(clientResponse), 0);
    }

  
    close(socketDescriptor);
    return 0;
}